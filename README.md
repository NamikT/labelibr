
<!-- README.md is generated from README.Rmd. Please edit that file -->

# labelibr

<!-- badges: start -->

<!-- badges: end -->

The goal of labelibr is to translate some codes to label.

## Installation

You can install the released version of labelibr from gitlab with:

``` r
devtools::install_gitlab("labelibr")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(labelibr)
## basic example code
labeleasier('920100039', Finess_to_LibGHU = T, F)
#> [1] "APHP.Nord-Université de Paris"
```
